<?php

namespace Koduliising\Liisi3\Block\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\View\Element\Template;
use Magento\Framework\View\Element\Template\Context;
use Koduliising\Liisi3\Model\Liisi3PaymentFactory;

class Start extends Template
{
    /**
     * @var Liisi3PaymentFactory
     */
    private $liisi3PaymentFactory;

    /**
     * @var Session
     */
    private $session;

    /**
     * Construct function.
     *
     * @param Context $context
     * @param Liisi3PaymentFactory $liisi3PaymentFactory
     * @param Session $session
     * @param array $data
     */
    public function __construct(
        Context $context,
        Liisi3PaymentFactory $liisi3PaymentFactory,
        Session $session,
        array $data = []
    ) {
        parent::__construct($context, $data);

        $this->liisi3PaymentFactory = $liisi3PaymentFactory;
        $this->session = $session;
    }

    /**
     * @return string
     */
    public function getFormActionUrl()
    {
        $payment = $this->liisi3PaymentFactory->create();
        $payment->getLiisi3API();

        return $payment->getUrl();
    }

    /**
     * @return array
     */
    public function getFormFields()
    {
        return $this->liisi3PaymentFactory->create()->getFormFields(
            $this->session->getLastRealOrder(),
            $this->getUrl('liisi3/payment/index')
        );
    }
}
