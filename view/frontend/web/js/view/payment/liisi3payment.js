/* global define */
define(['uiComponent', 'Magento_Checkout/js/model/payment/renderer-list'], function (Component, rendererList) {
        'use strict';

        rendererList.push({
            type: 'liisi3payment',
            component: 'Koduliising_Liisi3/js/view/payment/method-renderer/liisi3payment-method'
        });

        return Component;
    }
);
