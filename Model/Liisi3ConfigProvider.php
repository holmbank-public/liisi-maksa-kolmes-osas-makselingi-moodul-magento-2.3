<?php

namespace Koduliising\Liisi3\Model;

use Magento\Checkout\Model\ConfigProviderInterface;
use Magento\Framework\UrlInterface;
use Magento\Framework\View\Asset\Repository;

class Liisi3ConfigProvider implements ConfigProviderInterface
{
    /**
     * @var Repository
     */
    protected $_assetRepo;

    /**
     * @var UrlInterface
     */
    private $urlBuilder;

    /**
     * Construct function.
     *
     * @param Repository $assetRepo
     * @param UrlInterface $urlBuilder
     */
    public function __construct(Repository $assetRepo, UrlInterface $urlBuilder)
    {
        $this->_assetRepo = $assetRepo;
        $this->urlBuilder = $urlBuilder;
    }

    /**
     * {@inheritdoc}
     */
    public function getConfig()
    {
        $config = [
            'payment' => [
                'liisi3' => [
                    'paymentAcceptanceMarkSrc' => $this->_assetRepo->getUrl('Koduliising_Liisi3::images/liisi3-logo.png'),
                    'redirectUrl' => $this->urlBuilder->getUrl('liisi3/payment/start'),
                ],
            ],
        ];

        return $config;
    }
}
