<?php

namespace Koduliising\Liisi3\Controller\Payment;

use InvalidArgumentException;
use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\Controller\ResultFactory;

class Start extends Action
{
    /**
     * @var Session
     */
    private $session;

    /**
     * Construct function.
     *
     * @param Context $context
     * @param Session $session
     */
    public function __construct(Context $context, Session $session)
    {
        parent::__construct($context);

        $this->session = $session;
    }

    /**
     * @inheritdoc
     * @throws InvalidArgumentException
     */
    public function execute()
    {
        return $this->resultFactory->create(ResultFactory::TYPE_PAGE);
    }
}
