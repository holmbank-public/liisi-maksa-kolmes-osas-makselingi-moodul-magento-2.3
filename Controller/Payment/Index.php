<?php

namespace Koduliising\Liisi3\Controller\Payment;

use Magento\Checkout\Model\Session;
use Magento\Framework\App\Action\Action;
use Magento\Framework\App\Action\Context;
use Magento\Framework\App\CsrfAwareActionInterface;
use Magento\Framework\App\RequestInterface;
use Magento\Framework\App\Request\InvalidRequestException;
use Magento\Framework\Controller\Result\Redirect;
use Magento\Framework\Controller\ResultFactory;
use Magento\Framework\Controller\ResultInterface;
use Throwable;
use Koduliising\Liisi3\Helper\Response\Handler;
use Koduliising\Liisi3\Model\Liisi3Payment;

class Index extends Action implements CsrfAwareActionInterface
{
    /**
     * @var Session
     */
    private $checkoutSession;

    /**
     * @var Handler
     */
    private $responseHandlerHelper;

    /**
     * Construct function.
     *
     * @param Context $context
     * @param Handler $responseHandlerHelper
     * @param Session $checkoutSession
     */
    public function __construct(Context $context, Handler $responseHandlerHelper, Session $checkoutSession)
    {
        parent::__construct($context);

        $this->responseHandlerHelper = $responseHandlerHelper;
        $this->checkoutSession = $checkoutSession;
    }

    /**
     * @inheritdoc
     */
    public function execute()
    {
        if ($this->responseHandlerHelper->verifyRequest($request = $this->getRequest())) {
            if ($this->responseHandlerHelper->isAutoRequest($request)) {
                return $this->handleAutoRequest();
            }

            return $this->handleRedirectRequest();
        }

        return $this->resultRedirectFactory->create()->setPath('/');
    }

    /**
     * @return ResultInterface
     */
    private function handleAutoRequest()
    {
        $requestType = $this->responseHandlerHelper->getRequestType($this->getRequest());

        try {
            $response = $this->resultFactory->create(ResultFactory::TYPE_RAW);

            if ($requestType === Liisi3Payment::RESPONSE_1111) {
                $this->responseHandlerHelper->setOrderAsProcessing($this->getRequest());
            } else if ($requestType === Liisi3Payment::RESPONSE_1911) {
                $this->responseHandlerHelper->setOrderAsCancelled($this->getRequest());
            }

            return $response->setHttpResponseCode(200);
        } catch (Throwable $exception) {
            return $response->setHttpResponseCode(409);
        }
    }

    /**
     * @return Redirect
     */
    private function handleRedirectRequest()
    {
        $requestType = $this->responseHandlerHelper->getRequestType($this->getRequest());

        try {
            if ($requestType === Liisi3Payment::RESPONSE_1111) {
                $order = $this->responseHandlerHelper->loadOrderFromRequest($this->getRequest());

                $this->checkoutSession->setLastQuoteId($order->getQuoteId());
                $this->checkoutSession->setLastSuccessQuoteId($order->getQuoteId());
                $this->checkoutSession->setLastOrderId($order->getId());
                $this->checkoutSession->setLastRealOrderId($order->getIncrementId());
                $this->checkoutSession->setLastOrderStatus($order->getStatus());

                return $this->resultRedirectFactory->create()->setPath('checkout/onepage/success');
            } else {
                $this->responseHandlerHelper->setOrderAsCancelled($this->getRequest());
            }
        } catch (Throwable $exception) {
            return $this->resultRedirectFactory->create()->setPath('checkout/onepage/failure');
        }

        return $this->resultRedirectFactory->create()->setPath('checkout/onepage/failure');
    }

    /**
     * @inheritDoc
     */
    public function createCsrfValidationException(RequestInterface $request): ?InvalidRequestException
    {
        return null;
    }

    /**
     * @inheritDoc
     */
    public function validateForCsrf(RequestInterface $request): ?bool
    {
        return true;
    }
}
